EESchema Schematic File Version 2
LIBS:lts-underground-parts
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:lts-underground-cache
EELAYER 25 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 1
Title "Learn to Solder - Underground Studio"
Date "2016-10-28"
Rev "1"
Comp ""
Comment1 "James Bastow"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MPD_BK-913 BAT1
U 1 1 5813DFCA
P 4500 4300
F 0 "BAT1" H 4650 4350 50  0000 L CNN
F 1 "MPD_BK-913" H 4650 4250 50  0000 L CNN
F 2 "lts-underground-footprints:MPD_BK-913" H 4500 4370 60  0001 C CNN
F 3 "" H 4500 4370 60  0001 C CNN
	1    4500 4300
	1    0    0    -1  
$EndComp
$Comp
L LED D1
U 1 1 5813E05B
P 5700 4300
F 0 "D1" V 5700 4400 50  0000 C CNN
F 1 "LED" H 5700 4200 50  0001 C CNN
F 2 "lts-underground-footprints:LED-5MM" H 5700 4300 50  0001 C CNN
F 3 "" H 5700 4300 50  0000 C CNN
	1    5700 4300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4500 4200 4500 4000
Wire Wire Line
	4500 4000 6600 4000
Wire Wire Line
	5700 4000 5700 4100
Wire Wire Line
	4500 4400 4500 4600
Wire Wire Line
	4500 4600 6600 4600
Wire Wire Line
	5700 4600 5700 4500
$Comp
L LED D2
U 1 1 5813E248
P 6000 4300
F 0 "D2" V 6000 4400 50  0000 C CNN
F 1 "LED" H 6000 4200 50  0001 C CNN
F 2 "lts-underground-footprints:LED-5MM" H 6000 4300 50  0001 C CNN
F 3 "" H 6000 4300 50  0000 C CNN
	1    6000 4300
	0    -1   -1   0   
$EndComp
$Comp
L LED D3
U 1 1 5813E267
P 6300 4300
F 0 "D3" V 6300 4400 50  0000 C CNN
F 1 "LED" H 6300 4200 50  0001 C CNN
F 2 "lts-underground-footprints:LED-5MM" H 6300 4300 50  0001 C CNN
F 3 "" H 6300 4300 50  0000 C CNN
	1    6300 4300
	0    -1   -1   0   
$EndComp
$Comp
L LED D4
U 1 1 5813E289
P 6600 4300
F 0 "D4" V 6600 4400 50  0000 C CNN
F 1 "LED" H 6600 4200 50  0001 C CNN
F 2 "lts-underground-footprints:LED-5MM" H 6600 4300 50  0001 C CNN
F 3 "" H 6600 4300 50  0000 C CNN
	1    6600 4300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6000 4000 6000 4100
Connection ~ 5700 4000
Wire Wire Line
	6300 4000 6300 4100
Connection ~ 6000 4000
Wire Wire Line
	6600 4000 6600 4100
Connection ~ 6300 4000
Wire Wire Line
	6000 4600 6000 4500
Connection ~ 5700 4600
Wire Wire Line
	6300 4600 6300 4500
Connection ~ 6000 4600
Wire Wire Line
	6600 4600 6600 4500
Connection ~ 6300 4600
$Comp
L TEST_1P MH2
U 1 1 58176028
P 5700 5200
F 0 "MH2" H 5700 5470 50  0001 C CNN
F 1 "MH" H 5700 5400 50  0001 C CNN
F 2 "Mounting_Holes:MountingHole_2.5mm_Pad" H 5900 5200 50  0001 C CNN
F 3 "" H 5900 5200 50  0000 C CNN
	1    5700 5200
	1    0    0    -1  
$EndComp
$Comp
L TEST_1P MH1
U 1 1 58176243
P 5400 5200
F 0 "MH1" H 5400 5470 50  0001 C CNN
F 1 "MH" H 5400 5400 50  0001 C CNN
F 2 "Mounting_Holes:MountingHole_2.7mm_Pad" H 5600 5200 50  0001 C CNN
F 3 "" H 5600 5200 50  0000 C CNN
	1    5400 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 5200 5700 5200
Text Label 5500 5200 0    60   ~ 0
MH
$EndSCHEMATC
